<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    public $timestamps = false;
	
	/**
     * Get the jobs of a candidate.
     */
    public function jobs()
    {
        return $this->hasMany('App\Job');
    }
}
