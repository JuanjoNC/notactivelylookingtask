<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Candidate;
use App\Job;

class GetCandidates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'candidates:get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List all candidates with their jobs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $candidates = Candidate::all();

		foreach ($candidates as $candidate) {
			echo $candidate->id." - ".$candidate->name." ".$candidate->surname." - ".$candidate->email. PHP_EOL;
			
			foreach ($candidate->jobs->sortByDesc('end_date') as $job) {
				echo "\t".$job->title." in ".$job->company." (".$job->start_date." - ".$job->end_date.")". PHP_EOL;
			}
		}
    }
}
