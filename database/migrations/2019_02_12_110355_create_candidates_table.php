<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidates', function (Blueprint $table) {
            $table->increments('id');
			$table->string('name');
            $table->string('surname');
			$table->string('email');
        });
		
		DB::table('candidates')->insert(
			[
				['name' => 'Zola','surname' => 'Towne','email' => 'garrison40@example.com'],
				['name' => 'Stephania','surname' => 'Yundt','email' => 'qritchie@example.com'],
				['name' => 'Margret','surname' => 'Reichert','email' => 'ralph.harber@example.org'],
				['name' => 'Mozell','surname' => 'Jones','email' => 'walsh.tia@example.com'],
				['name' => 'Destiny','surname' => 'Emmerich','email' => 'ybraun@example.com'],
				['name' => 'Cortney','surname' => 'McDermott','email' => 'cordia.douglas@example.net'],
				['name' => 'Melvin','surname' => 'Crooks','email' => 'enos.goodwin@example.net'],
				['name' => 'Mack','surname' => 'Ferry','email' => 'phand@example.org'],
				['name' => 'Fanny','surname' => 'Deckow','email' => 'kblick@example.org'],
				['name' => 'Katarina','surname' => 'Ebert','email' => 'shawna25@example.net']
			]	
		);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidates');
    }
}
