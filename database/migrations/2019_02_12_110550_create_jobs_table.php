<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');
			$table->unsignedInteger('candidate_id');
			$table->string('title');
            $table->string('company');
			$table->datetime('start_date');
			$table->datetime('end_date');
			$table->foreign('candidate_id')->references('id')->on('candidates');
        });
		
		DB::table('jobs')->insert(
			[
				['candidate_id' => 1, 'title' => 'Accountant', 'company' => 'Software Ridge', 'start_date' => date('2016-05-20 10:31:00'), 'end_date' => date('2019-02-07 15:20:00')],
				['candidate_id' => 1, 'title' => 'Actor', 'company' => 'Software Dev', 'start_date' => date('2016-05-20 08:23:00'), 'end_date' => date('2019-02-07 15:20:00')],
				['candidate_id' => 1, 'title' => 'Actuary', 'company' => 'Software Inc', 'start_date' => date('2017-04-18 08:31:00'), 'end_date' => date('2019-02-07 15:20:00')],
				['candidate_id' => 2, 'title' => 'Administrative Services Manager', 'company' => 'Software Rank', 'start_date' => date('2016-04-29 14:13:00'), 'end_date' => date('2019-02-07 15:20:00')],
				['candidate_id' => 2, 'title' => 'High School Teachers', 'company' => 'Software Vine', 'start_date' => date('2016-12-14 09:55:00'), 'end_date' => date('2019-02-07 15:20:00')],
				['candidate_id' => 2, 'title' => 'Advertising Sales Agent', 'company' => 'Ridge Software', 'start_date' => date('2015-01-14 23:53:00'), 'end_date' => date('2019-02-07 15:20:00')],
				['candidate_id' => 3, 'title' => 'Advertising', 'company' => 'Dev Software', 'start_date' => date('2015-01-14 23:54:00'), 'end_date' => date('2019-02-07 15:20:00')],
				['candidate_id' => 3, 'title' => 'Operations Technician', 'company' => 'Inc Software', 'start_date' => date('2015-01-14 23:54:00'), 'end_date' => date('2019-02-07 15:20:00')],
				['candidate_id' => 3, 'title' => 'Aerospace Engineer', 'company' => 'Rank Software', 'start_date' => date('2015-01-14 23:52:00'), 'end_date' => date('2019-02-07 15:19:00')],
				['candidate_id' => 4, 'title' => 'Food Science Technician', 'company' => 'Vine Software', 'start_date' => date('2015-01-14 23:52:00'), 'end_date' => date('2019-02-07 15:19:00')],
				['candidate_id' => 4, 'title' => 'Agricultural Scientist', 'company' => 'Software Ability', 'start_date' => date('2015-01-14 23:51:00'), 'end_date' => date('2019-02-07 15:19:00')],
				['candidate_id' => 4, 'title' => 'Agricultural Engineer', 'company' => 'Software Ex', 'start_date' => date('2015-01-14 23:54:00'), 'end_date' => date('2019-02-07 15:20:00')],
				['candidate_id' => 5, 'title' => 'Agricultural Worker', 'company' => 'Software Industries', 'start_date' => date('2015-01-14 23:54:00'), 'end_date' => date('2019-02-07 15:20:00')],
				['candidate_id' => 5, 'title' => 'Air Traffic Controllers', 'company' => 'Software Zoom', 'start_date' => date('2015-01-14 23:55:00'), 'end_date' => date('2019-02-07 15:20:00')],
				['candidate_id' => 5, 'title' => 'Service Technician', 'company' => 'Software Rhino', 'start_date' => date('2015-01-14 23:50:00'), 'end_date' => date('2019-02-07 15:19:00')],
				['candidate_id' => 6, 'title' => 'Aircraft Pilot', 'company' => 'Ability Software', 'start_date' => date('2015-01-14 23:52:00'), 'end_date' => date('2019-02-07 15:19:00')],
				['candidate_id' => 6, 'title' => 'Animal Care and Service Worker', 'company' => 'Ex Software', 'start_date' => date('2015-01-14 23:53:00'), 'end_date' => date('2019-02-07 15:20:00')],
				['candidate_id' => 6, 'title' => 'Announcer', 'company' => 'Industries Software', 'start_date' => date('2015-01-14 23:54:00'), 'end_date' => date('2019-02-07 15:20:00')],
				['candidate_id' => 7, 'title' => 'Anthropologist', 'company' => 'Zoom Software', 'start_date' => date('2015-01-14 23:54:00'), 'end_date' => date('2019-02-07 15:20:00')],
				['candidate_id' => 7, 'title' => 'Appraiser of Real Estate', 'company' => 'Rhino Software', 'start_date' => date('2015-01-14 23:50:00'), 'end_date' => date('2019-02-07 15:19:00')],
				['candidate_id' => 7, 'title' => 'Mediator', 'company' => 'Software Able', 'start_date' => date('2015-01-14 23:50:00'), 'end_date' => date('2019-02-07 15:19:00')],
				['candidate_id' => 8, 'title' => 'Architect', 'company' => 'Software Bit', 'start_date' => date('2015-01-14 23:51:00'), 'end_date' => date('2019-02-07 15:19:00')],
				['candidate_id' => 8, 'title' => 'Architectural and Engineering Manager', 'company' => 'Software Ink', 'start_date' => date('2015-01-14 23:54:00'), 'end_date' => date('2019-02-07 15:20:00')],
				['candidate_id' => 8, 'title' => 'Museum Technician', 'company' => 'Software Ment', 'start_date' => date('2015-01-14 23:51:00'), 'end_date' => date('2019-02-07 15:19:00')],
				['candidate_id' => 9, 'title' => 'Art Director', 'company' => 'Software Gorilla', 'start_date' => date('2015-01-14 23:54:00'), 'end_date' => date('2019-02-07 15:20:00')],
				['candidate_id' => 9, 'title' => 'Artist', 'company' => 'Able Software', 'start_date' => date('2015-01-14 23:52:00'), 'end_date' => date('2019-02-07 15:19:00')],
				['candidate_id' => 9, 'title' => 'Assembler', 'company' => 'Software Amp', 'start_date' => date('2015-01-14 23:52:00'), 'end_date' => date('2019-02-07 15:19:00')],
				['candidate_id' => 10, 'title' => 'Athlete', 'company' => 'Software Pin', 'start_date' => date('2015-01-14 23:51:00'), 'end_date' => date('2019-02-07 15:19:00')],
				['candidate_id' => 10, 'title' => 'Athletic Trainer', 'company' => 'Software Velocity', 'start_date' => date('2015-01-14 23:54:00'), 'end_date' => date('2019-02-07 15:20:00')],
				['candidate_id' => 10, 'title' => 'Atmospheric Scientist', 'company' => 'Software Woop', 'start_date' => date('2015-01-14 23:54:00'), 'end_date' => date('2019-02-07 15:20:00')]
			]	
		);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
